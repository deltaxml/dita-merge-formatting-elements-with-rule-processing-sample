// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.ditamerge.ConcurrentMerge;
import com.deltaxml.ditamerge.ConcurrentMerge.MergeResultType;
import com.deltaxml.ditamerge.RuleConfiguration;
import com.deltaxml.ditamerge.FormattingElementsConfiguration;

import java.util.Arrays;
import java.util.HashSet;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Set;
import java.util.TreeSet;

public class FormattingElementsWithRuleProcessing {
  public static void main(String[] args) throws Exception {
    File samplesResultFolder = new File("results/");

    // Ancestor (A) and Version (B,C,D) files
    File aVersion = new File("FEInput1-A.xml");
    File bVersion = new File("FEInput1-B.xml");
    File cVersion = new File("FEInput1-C.xml");

    // Result File
    File resultFile = new File("result.xml");
    
    System.out.println("Running Concurrent Merge....");
    ConcurrentMerge merge= new ConcurrentMerge();
    
    //specifying the formatting elements list and override or update the default list
    FormattingElementsConfiguration formattingElementsConfiguration= new FormattingElementsConfiguration();
    formattingElementsConfiguration.setFormattingElements(new HashSet<>(Arrays.asList("b", "i", "sup", "sub", "tt", "u", "line-through", "overline")));
    formattingElementsConfiguration.enableFormattingElements(true);
    merge.setFormattingElementsConfiguration(formattingElementsConfiguration);
     
    merge.setAncestor(aVersion, "A");
    System.out.println("Ancestor (\"A\") set");
    
    merge.addVersion(bVersion, "B");
    System.out.println("Version \"B\" added");
    
    merge.addVersion(cVersion, "C");
    System.out.println("Version \"C\" added");
    
    System.out.println("Extracting DeltaV2.1 format output without rule processing");
    merge.extractAll(new File(samplesResultFolder, "deltav21.xml"));
    
    System.out.println("Extracting DeltaV2.1 format output with rule processing");
    merge.setResultType(MergeResultType.RULE_PROCESSED_DELTAV2);
    merge.extractAll(new File(samplesResultFolder, "deltav21-rule-processed.xml"));
    
    System.out.println("Extracting DeltaV2.1 format output with rule processing - Display Changes Involving Version B");
    RuleConfiguration rc= new RuleConfiguration();
    Set<String> involving= new TreeSet<>();
    involving.add("B");
    rc.setDisplayChangesInvolving(involving);
    merge.setRuleConfiguration(rc);
    merge.extractAll(new File(samplesResultFolder, "deltav21-rule-processed-changes-involving-B.xml"));

    System.out.println("Extracting DeltaV2.1 format output with rule processing - Display Formatting Changes In all 'p' eleemnts");
    RuleConfiguration rc1= new RuleConfiguration();
    rc1.setDisplayFormatChangesIn("//*[local-name() eq 'p']");
    merge.setRuleConfiguration(rc1);
    merge.extractAll(new File(samplesResultFolder, "deltav21-rule-processed-formatting-changes-changes-in.xml"));

    System.out.println("Result files created at: " + samplesResultFolder.getCanonicalPath());
    System.out.println("");
    
  }
}
